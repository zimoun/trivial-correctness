module CheatGlue where

import qualified Parser    as P
import qualified CheatGenerated as Coq

push :: P.STree -> Coq.Tree
push (P.Val x)   = Coq.Val $ x
push (P.Add x y) = Coq.Add (push x) (push y)

pop :: Coq.Stack -> Integer
pop = head


dissamble :: Coq.Binary -> String
dissamble ((Coq.PUSH n) : rest) = "PUSH " ++ show n ++ ", " ++ dissamble rest
dissamble ((Coq.ADD)    : rest) = "ADD"             ++ ", " ++ dissamble rest
dissamble []                    = ";"
