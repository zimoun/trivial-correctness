module Main where

import CmdLine (cmd)
import Parser (STree(..))

-- | Interesting bits | --

go = pop . no_verified . push

no_verified = emulator . compiler

-- | Emulator

data Instr = PUSH Integer | ADD
  deriving (Show)

type Binary = [Instr]
type Stack  = [Integer]

exec :: Binary -> Stack -> Stack
exec (PUSH n : rest) stack        = exec rest (n : stack)
exec (ADD : rest) (m : n : stack) = exec rest (m + n : stack)
exec [] stack = stack
exec _ stack  = stack

emulator :: Binary -> Stack
emulator b = exec b []

-- | Compiler

comp :: STree -> Binary -> Binary
comp (Val n) bytecode   = PUSH n : bytecode
comp (Add x y) bytecode = comp x $ comp y $ ADD : bytecode

compiler :: STree -> Binary
compiler source = comp source []

-- | Layer compatibility (useless)

push = id
pop  = head

dissamble :: Binary -> String
dissamble (PUSH n : rest) = (show $ PUSH n) ++ ", " ++ dissamble rest
dissamble (ADD    : rest) = (show ADD)      ++ ", " ++ dissamble rest
dissamble [] = ";"

emit :: STree -> String
emit = dissamble . compiler

-- | Usual command-line interface | --

main = cmd go emit
