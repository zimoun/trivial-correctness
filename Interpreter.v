Require Import Lang.

Fixpoint eval (source:Tree) :=
  match source with
    Val n => n
  | Add x y => (eval x) + (eval y)
  end.
