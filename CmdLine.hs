module CmdLine where

import System.Environment (getArgs)
import System.IO (hFlush, stdout)
import System.Exit (exitWith, ExitCode(..))
import Parser (parser)


-- | Usual command-line interface | --

cmd f c = getArgs >>= (usage f c) >>= putStr

options = "Usage: calc -x '1+2' | -i | -c '1+2'"

usage _ _ ["-h"]     = putStrLn    options                 >> exit
usage f _ ["-i"]     = interactive f                       >> exit
usage f _ ("-x":s:_) = putStrLn    (internal f s)          >> exit
usage _ c ("-c":s:_) = putStrLn    (emit c s)              >> exit
usage _ _ _          = putStrLn    "Try: -x '(1+2)+(3+4)'" >> die

exit = exitWith ExitSuccess
die  = exitWith $ ExitFailure 1

interactive f = do
  putStrLn "Control D ends the interactive session."
  loop
    where loop = do
            putStr "calc> "
            hFlush stdout
            s <- getLine
            putStrLn $ internal f s
            loop

internal go s = case parser s of
  Nothing   -> "Expression incorrect: " ++ s
  Just tree -> "= " ++ (show $ go tree)

emit compil s = case parser s of
  Nothing   -> "Expression incorrect: " ++ s
  Just tree -> "Instructions: " ++ (show $ compil tree)
