Require Import Lang.
Require Import Compiler Emulator.
Require Import Interpreter.

Require Import Arith.
Require Import List.
Import ListNotations.


Lemma correctness_comp: forall (code:Tree) (bytecode:Binary) (stack:Stack),
    exec (comp code bytecode) stack = exec bytecode (eval code :: stack).
Proof.
  intros code.
  induction code.
  - simpl. reflexivity.
  - unfold comp. fold comp.
    intros bytecode stack.
    rewrite IHcode1.
    rewrite IHcode2.
    unfold eval. fold eval.
    unfold exec. fold exec.
    rewrite Nat.add_comm.
    reflexivity.
Qed.

Corollary correctness_comp': forall (code:Tree) (stack:Stack),
    exec (comp code []) stack = eval code :: stack.
Proof.
  intros code stack.
  rewrite correctness_comp.
  unfold exec.
  reflexivity.
Qed.

Theorem correctness': forall (code:Tree) (stack:Stack),
    exec (compiler code) stack = eval code :: stack.
Proof.
  intros code stack.
  unfold compiler.
  rewrite correctness_comp'.
  reflexivity.
Qed.

Corollary correctness'': forall (code:Tree),
    exec (compiler code) [] = eval code :: [].
Proof.
  intro code.
  rewrite correctness'.
  reflexivity.
Qed.

Theorem correctness: forall (code:Tree),
    emulator (compiler code) = eval code :: [].
Proof.
  intro code.
  unfold emulator.
  rewrite correctness''.
  reflexivity.
Qed.


(* compiler' is also correct *)
(* It is harder because it requires first a non-trivial lemma *)

Lemma distributivity: forall (s:Tree) (b:Binary) (stack:Stack),
    exec (compiler' s ++ b) stack = exec b (eval s :: stack).
Proof.
  intros s.
  induction s.
  - unfold compiler'. unfold eval.
    unfold exec at 1.
    simpl.
    fold exec.
    reflexivity.
  - unfold compiler'. fold compiler'.
    unfold eval. fold eval.
    intros b stack.
    rewrite app_assoc_reverse.
    rewrite IHs1.
    rewrite app_assoc_reverse.
    rewrite IHs2.
    unfold exec at 1. simpl. fold exec.
    rewrite Nat.add_comm.
    reflexivity.
Qed.

Theorem correctness_compiler': forall (code:Tree) (stack:Stack),
    exec (compiler' code) stack = eval code :: stack.
Proof.
  intros code.
  induction code.
  - simpl. reflexivity.
  - unfold compiler'. simpl. fold compiler'.
    intros stack.
    rewrite distributivity.
    rewrite distributivity.
    unfold exec.
    rewrite Nat.add_comm.
    reflexivity.
Qed.
