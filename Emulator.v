Require Import Lang.

Require Import List.
Import ListNotations.

Inductive Instr:Type := PUSH (n:nat)
                      | ADD.

Definition Binary := list Instr.
Definition Stack := list nat.


Fixpoint exec (b:Binary) (s:Stack) :=
  match b, s with
    PUSH n :: rest, stack => exec rest (n :: stack)
  | ADD :: rest, m :: n :: stack => exec rest (m + n :: stack)
  | [], stack => stack
  | _, stack => stack
  end.

Definition emulator (b:Binary) := exec b [].
