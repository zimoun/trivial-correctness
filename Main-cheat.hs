module Main where


import CmdLine (cmd)
import CheatGlue ( push, pop
                 , dissamble)
import CheatGenerated (compiler, emulator)

-- | Interesting bits | --

go = pop . cheat_verified . push

cheat_verified = emulator . compiler

-- push     :: STree      -> Coq.Tree
-- compiler :: Coq.Tree   -> Coq.Binary
-- emulator :: Coq.Binary -> Coq.Stack
-- pop      :: Coq.Stack  -> Integer

-- The cheat comes from 'emulator':
-- it uses 'nat' in Coq but 'Integer' in Haskell.
-- Both types do not match, as explained at length by Coq documentation.
-- Coq 'nat' represents (theoretically) all natural numbers (infinite).
-- Haskell 'Integer' represents some bounded relative integers.
-- Theorefore, theorems could hold in Coq but not in Haskell.


emit = dissamble . compiler . push

main = cmd go emit
