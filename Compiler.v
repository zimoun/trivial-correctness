Require Import Lang.
Require Import Emulator.

Require Import List.
Import ListNotations.

(* XXXX: Read Coq System Module,         *)
(* because lang.Val/Add seems avoidable. *)


Fixpoint comp (source:Tree) (bytecode:Binary):=
  match source with
    Lang.Val n => PUSH n :: bytecode
  | Lang.Add x y => comp x (comp y (ADD :: bytecode))
  end.

Definition compiler (source:Tree) := comp source [].


(* Similar but harder to proof *)
Fixpoint compiler' (source:Tree) :=
  match source with
    Lang.Val n => [PUSH n]
  | Lang.Add x y => compiler' x ++ compiler' y ++ [ADD]
  end.
