Require Export Lang Compiler Emulator.

Require Extraction.
Extraction Language Haskell.

Extract Inductive list => "[]" [ "[]" "(:)" ].
Extract Inductive nat => "Prelude.Integer" [ "0" "Prelude.succ" ]
                                           "(\fO fS n -> if n Prelude.== 0 then fO () else fS (Prelude.pred n))".
Extract Constant plus => "(Prelude.+)".

Extraction "CheatGenerated.hs" compiler emulator.
