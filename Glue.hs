module Glue where

import qualified Parser    as P
import qualified Generated as Coq

int2nat :: Integer -> Coq.Nat
int2nat 0 = Coq.O
int2nat n
  | n >= 1     = Coq.S $ int2nat (n -1)
  | otherwise = Coq.O

push :: P.STree -> Coq.Tree
push (P.Val x)   = Coq.Val $ int2nat x
push (P.Add x y) = Coq.Add (push x) (push y)


nat2int :: Coq.Nat -> Integer
nat2int Coq.O     = 0
nat2int (Coq.S n) = 1 + nat2int n

pop :: Coq.Stack -> Integer
pop (Coq.Cons x Coq.Nil) = nat2int x
pop _                    = 0


dissamble :: Coq.Binary -> String
dissamble (Coq.Cons (Coq.PUSH n) rest) = "PUSH " ++ show (nat2int n) ++ ", " ++ dissamble rest
dissamble (Coq.Cons (Coq.ADD)    rest) = "ADD"                       ++ ", " ++ dissamble rest
dissamble Coq.Nil                      = ";"
