
# Test parameters
ARGS='(1+2)+(3+(4+0))'
OPTS=+RTS -t

OUCH='(1+2)+(3+(4+10000000))'


all: gen bin verif run
	$(info Done.)


gen: Extraction.vo
	$(info Compiler generated.)

bin: calc
	$(info Trivial calculator compiled.)

verif: Verifications.vo
	$(info Trivial calculator verified.)

others: calc-no-coq calc-cheat
	$(info Other implementations compiled.)

run: bin others
	$(info Comparison:)
	./calc        -x ${ARGS} ${OPTS}
	./calc-no-coq -x ${ARGS} ${OPTS}
	./calc-cheat  -x ${ARGS} ${OPTS}

	./calc        -c ${ARGS} ${OPTS}
	./calc-no-coq -c ${ARGS} ${OPTS}
	./calc-cheat  -c ${ARGS} ${OPTS}

ouch: bin others
	$(info OUCH!)
	./calc        -x ${OUCH} ${OPTS}
	./calc-no-coq -x ${OUCH} ${OPTS}
	./calc-cheat  -x ${OUCH} ${OPTS}

	./calc        -c ${OUCH} ${OPTS}
	./calc-no-coq -c ${OUCH} ${OPTS}
	./calc-cheat  -c ${OUCH} ${OPTS}

Extraction.vo: Extraction.v Lang.vo Compiler.vo Emulator.vo
	coqc $<

CheatExtraction.vo: CheatExtraction.v Lang.vo Compiler.vo Emulator.vo
	coqc $<

Lang.vo: Lang.v
	coqc $<

Interpreter.vo: Interpreter.v Lang.vo
	coqc $<

Compiler.vo: Compiler.v Lang.vo Emulator.vo
	coqc $<

Emulator.vo: Emulator.v Lang.vo
	coqc $<

Verifications.vo: Verifications.v Extraction.vo Interpreter.vo
	coqc $<

clean:
	$(info Require 'rm' (see coreutils).)
	-rm *.vo *.vos *.vok *.glob
	-rm .[A-Z]*.aux
	-rm Generated.hs
	-rm *.o *.hi
	-rm calc
	-rm calc-no-coq
	-rm calc-cheat
	-rm CheatGenerated.hs

#
# Poor Haskell compilation
#
# XXXX: Use cabal or similar.
# Because the Makefile rule triggers recompilations,
# only if sources are modified (Main* or *Extraction),
# and not when other Haskell dependencies are,
# although 'ghc --make' correctly does it.
# Too boring to add by hand all dependencies.
#

# At best, relinks all.
rm:				# Ugly
	-rm calc calc-cheat calc-no-coq
redo: rm bin others		# Dirty
	$(info Redone.)

calc: Main.hs Extraction.vo
	ghc --make $< -o $@ -rtsopts

calc-cheat: Main-cheat.hs CheatExtraction.vo
	ghc --make $< -o $@ -rtsopts

calc-no-coq: Main-no-coq.hs
	ghc --make $< -o $@ -rtsopts
