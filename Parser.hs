{-# LANGUAGE FlexibleContexts #-}

module Parser ( parser
              , STree(..)
              ) where

import Prelude hiding (subtract)
import Text.Parsec
import Text.Parsec.String (Parser)

data STree = Val Integer
          | Add STree STree
  deriving (Show)


parser :: String -> Maybe STree
parser input = case parse expr "" input of
                    Left _  -> Nothing
                    Right e -> Just e


expr :: Parser STree
expr = _parens_OR_value_ `chainl1` addition

_parens_OR_value_ :: Parser STree
_parens_OR_value_ = between spaces spaces $ parens <|> value

value :: Parser STree
value = fmap val digits
  where
    val = Val . read
    digits = many1 digit

parens :: Parser STree
parens = between (char '(') (char ')') expr

addition :: Parser (STree -> STree -> STree)
addition = fmap (const add) plus
  where
    -- | XXXX: const is required. Why?
    add a b =  Add a  b
    plus = char '+'
