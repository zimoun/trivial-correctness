module Main where

import CmdLine (cmd)
import Glue ( push, pop
            , dissamble)
import Generated (compiler, emulator)

-- | Interesting bits | --

go = pop . formally_verified . push

formally_verified = emulator . compiler

-- push     :: STree      -> Coq.Tree
-- compiler :: Coq.Tree   -> Coq.Binary
-- emulator :: Coq.Binary -> Coq.Stack
-- pop      :: Coq.Stack  -> Integer


emit = dissamble . compiler . push

main = cmd go emit
